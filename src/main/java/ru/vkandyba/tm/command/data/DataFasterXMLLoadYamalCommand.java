package ru.vkandyba.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterXMLLoadYamalCommand extends AbstractDataCommand{

    @Override
    public String name() {
        return "data-fasterxml-load-yamal";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Data FasterXML save Yamal ...";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(FILE_YAML_FASTERXML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }
}
