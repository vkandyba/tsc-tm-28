package ru.vkandyba.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public Task findByName(@NotNull String userId, @NotNull String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .filter(e -> name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull String userId, @NotNull String name) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task startById(@NotNull String userId, @NotNull String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Nullable
    @Override
    public Task startByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Nullable
    @Override
    public Task startByName(@NotNull String userId, @NotNull String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Nullable
    @Override
    public Task finishById(@NotNull String userId, @NotNull String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Nullable
    @Override
    public Task finishByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Nullable
    @Override
    public Task finishByName(@NotNull String userId, @NotNull String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        @Nullable final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Nullable
    @Override
    public Task unbindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        @Nullable final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        List<Task> listByProject = new ArrayList<>();
        List<Task> taskList = findAll(userId);
        for (Task task : taskList) {
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        return listByProject;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        for (Task task : listByProject) {
            list.remove(task);
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        list.removeAll(findAll(userId));
    }

}
